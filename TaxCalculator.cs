﻿using System.Collections.Generic;
using System;

namespace TaxCalculations
{
    public class TaxCalculator
    {
        private static IList<TaxBounds> TaxBands2023 = new List<TaxBounds> {
            new TaxBounds(){ Lower = 0, Upper = 12570, Rate = 0M },
            new TaxBounds(){ Lower = 12571, Upper = 50270, Rate = 0.2M },
            new TaxBounds(){ Lower = 50271, Upper = 150000, Rate = 0.4M },
            new TaxBounds(){ Lower = 150001, Upper = int.MaxValue, Rate = 0.45M }
        };
        public static decimal CalculateAnnualTax(decimal gross)
        {
            var totalTax = 0M;
            foreach(var taxBand in TaxBands2023){
                var taxForBand = (Math.Min(taxBand.Upper, gross) - taxBand.Lower + 1) * taxBand.Rate;
                totalTax += taxForBand > 0 ? taxForBand : 0;
                if(gross <= taxBand.Upper) break; // don't keep looping if salary below next tax bound
            }
            return totalTax;
        }
    }
}
