﻿namespace TaxCalculations
{
    public class TaxBounds
    {
        public int Lower { get; set; }
        public int Upper { get; set; }
        public decimal Rate { get; set; }
    }
}