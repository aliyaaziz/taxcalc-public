﻿using NUnit.Framework;

namespace TaxCalculations
{
    [TestFixture]
    public class SolutionTest
    {
        [Test]
        public void NoTax()
        {
            Assert.That(TaxCalculator.CalculateAnnualTax(12570), Is.EqualTo(0));
        }

        [Test]
        public void TwentyPercent()
        {
            Assert.That(TaxCalculator.CalculateAnnualTax(50000), Is.EqualTo(7486));
        }

        [Test]
        public void FortyPercent()
        {
            Assert.That(TaxCalculator.CalculateAnnualTax(65000), Is.EqualTo(13432));
        }

        [Test]
        public void FortyFivePercent()
        {
            Assert.That(TaxCalculator.CalculateAnnualTax(155000), Is.EqualTo(54710)); // zero personal allowance
        }
    }
}