﻿# Code Kata - Assessment

### Tax Calculation
- The tax calculation seems to be not working in this proof of concept and needs to work as follows on employees **annual gross** salary:
 
> - The tax brackets are stacking and you're expected to continue applying the brackets until no more brackets or salary are left (whichever comes first)  
> - All brackets are inclusive ranges 

    - 0-12500: 0%
    - 12501-50000: 20%
    - 50001-150000: 40%
    - 150001 or above: 45%

**Example**

    Annual salary: 85000
    Next tax bracket upper bound: 12500
    Tax: 0
    
*Apply tax bracket*
> Calculate tax for bracket: ([Min(Bracket upper bound of Tax, Annual Salary)] - [Bracket lower bound]) * [Bracket tax rate]*

    Tax: (12500 - 0 + 1) * 0% => 0
    Next tax bracket upper bound: 50000
    
*Apply tax bracket*

    Tax: (50000 - 12501 + 1) * 20% => 7500
    Next tax bracket upper bound: 50000

*Apply tax bracket*
 
    Tax: (85000 - 50001 + 1) * 40% => 14000
    Next tax bracket upper bound: N/A

    Total Tax Due: 0 + 7500 + 14000 = 21500
